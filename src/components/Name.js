import React from "react";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {setName} from "../actions/nameActions";

class Name extends React.Component {
    // constructor(props){
    //     super(props);
    //     this.state={
    //         name:'朋友'
    //     };
    // }
    handleChange(e) {
        const name = e.target.value;

        this.props.setName(name);
    }

    render() {
        const{name}=this.props.nameReducer;
        return (
            <section className="name">
                <input type="text" onChange={this.handleChange.bind(this)}/>
                <h1>Hello , {name}!</h1>
            </section>
        )
    }
}

const mapDispatchToProps = (dispatch) => bindActionCreators({
    setName
}, dispatch);

const mapStateToProps = (state)=>({
    nameReducer: state.nameReducer
});

export default connect(mapStateToProps, mapDispatchToProps)(Name);

