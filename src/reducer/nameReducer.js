const initialState={
    name:'friend',
};
export default (state=initialState,action)=>{
    switch (action.type) {
        case 'SET_NAME':
            return{
                ...state,
                name:action.name
            };
        default:
            return state;
    }
}
